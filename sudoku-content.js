class SudokuContent {
  constructor() {
    const validChallengeContent = this.resolveChallengeExample();
    const invalidDuplicateDigits = this.resolveInvalidDuplicateDigits();
    const invalidRowLength = this.resolveInvalidRowLength();
    const invalidCharacters = this.resolveInvalidCharacters();

    this.tableContent = [
      validChallengeContent,
      invalidDuplicateDigits,
      invalidRowLength,
      invalidCharacters
    ];
  }

  resolveChallengeExample() {
    return [
      ["4", "1", "9", ".", "8", ".", ".", ".", "."],
      ["5", ".", "8", ".", ".", ".", ".", ".", "6"],
      [".", ".", ".", "5", ".", ".", ".", ".", "."],
      [".", "9", ".", "6", ".", ".", ".", ".", "4"],
      [".", "4", ".", ".", ".", ".", ".", ".", "3"],
      ["6", ".", ".", "2", "9", ".", ".", "8", "."],
      [".", ".", "2", "3", ".", "1", ".", ".", "."],
      [".", ".", ".", ".", ".", "9", "2", "5", "."],
      [".", "7", ".", ".", ".", ".", ".", ".", "."]
    ];
  }

  resolveInvalidDuplicateDigits() {
    return [
      ["4", "1", "9", ".", "8", ".", ".", "4", "3"],
      ["5", ".", "8", ".", ".", ".", ".", ".", "6"],
      [".", ".", ".", "5", ".", ".", ".", ".", "3"],
      [".", "9", ".", "6", ".", ".", ".", ".", "4"],
      [".", "4", ".", ".", ".", ".", "2", ".", "3"],
      ["6", ".", ".", "2", "9", ".", ".", "8", "3"],
      ["1", ".", "2", "3", ".", "1", ".", ".", "1"],
      [".", ".", ".", ".", ".", "9", "2", "5", "."],
      [".", "7", ".", ".", ".", ".", ".", ".", "."]
    ];
  }

  resolveInvalidRowLength() {
    return [
      ["4", "1", "9", ".", "8", ".", ".", ".", "."],
      ["5", ".", "8", ".", ".", ".", ".", ".", "6", "1"],
      [".", ".", ".", "5", ".", ".", ".", "."],
      [".", "9", ".", "6", ".", ".", ".", ".", "4"],
      [".", "4", ".", ".", ".", ".", ".", ".", "3", "2"],
      ["6", ".", ".", "2", "9", ".", ".", "8", "."],
      [".", ".", "2", "3", ".", "1", ".", "."],
      [".", ".", ".", ".", ".", "9", "2", "5", ".", "3"],
      [".", "7", ".", ".", ".", ".", ".", ".", "."]
    ];
  }

  resolveInvalidCharacters() {
    return [
      ["4", "I", "$", ".", "N", ".", ".", ".", "."],
      ["5", ".", "8", ".", ".", ".", ".", ".", "6"],
      [".", ".", ".", "!", ".", ".", ".", ".", "."],
      [".", "V", "@", "A", ".", ".", ".", ".", "%"],
      [".", "L", ".", ".", ".", ".", ".", ".", "*"],
      ["6", ".", ".", "I", "9", ".", ".", "+", "."],
      [".", ".", "2", "3", ".", "1", ".", ".", "."],
      [".", ".", ".", ".", ".", "9", "2", "D", "."],
      [".", "7", ".", ".", ".", ".", ".", ".", "."]
    ];
  }
}

const sudokuContent = new SudokuContent();
