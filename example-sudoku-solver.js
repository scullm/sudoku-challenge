// I originally started to put something together myself out of curiousity, but then I got lazy and started to do some research.
// It seemed that a lot of developers out there were going with backtracking algorithms.
// I wanted to keep stuff simple for the user and also for myself so I could extend it to e.g. validate and report errors.
// This is based on an example I found on stackoverflow
// I choose it because it relatively simple one to enhance.

class ExampleSudokuSolver {
  constructor() {
    this.solvedTable;
  }
  isValidCellResult(board, row, col, k) {
    for (let i = 0; i < 9; i++) {
      const m = 3 * Math.floor(row / 3) + Math.floor(i / 3);
      const n = 3 * Math.floor(col / 3) + (i % 3);
      if (board[row][i] == k || board[i][col] == k || board[m][n] == k) {
        return false;
      }
    }
    return true;
  }

  solveTable(data) {
    for (let i = 0; i < 9; i++) {
      for (let j = 0; j < 9; j++) {
        if (data[i][j] == ".") {
          for (let k = 1; k <= 9; k++) {
            if (this.isValidCellResult(data, i, j, k)) {
              data[i][j] = `${k}`;
              if (this.solveTable(data)) {
                return true;
              } else {
                data[i][j] = ".";
              }
            }
          }
          return false;
        }
      }
    }
    this.solvedTable = data;

    return true;
  }
}

const exampleSudokuSolver = new ExampleSudokuSolver();
