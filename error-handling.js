class ErrorHandling {
  resolveErrorContent(errorReport) {
    return this.resolveErrorContentByValidationType(errorReport);
  }

  resolveErrorContentByValidationType(errorReport) {
    const errorContentByValidationType = {
      uniqueDigitsValidation: errorData => {
        const reportItem = {
          errorName: "Duplicate Digits",
          errorMessages: []
        };
        Object.keys(errorData.uniqueDigitsValidation).forEach(errorDataItem => {
          const subjectSplit = errorDataItem.split("_");
          const objectSplit = Object.keys(
            errorData.uniqueDigitsValidation[errorDataItem]
          )[0].split("_");
          const firstWord =
            subjectSplit[0].charAt(0).toUpperCase() + subjectSplit[0].slice(1);
          const error = `${firstWord} ${subjectSplit[1]} contains ${
            errorData.uniqueDigitsValidation[errorDataItem][
              Object.keys(errorData.uniqueDigitsValidation[errorDataItem])[0]
            ].length
          } occurrences of the number ${objectSplit[1]}`;
          reportItem.errorMessages.push(error);
        });
        return reportItem;
      },
      rowLengthValidation: errorData => {
        const reportItem = {
          errorName: "Invalid Row Length",
          errorMessages: []
        };
        errorData.rowLengthValidation.invalidRowIndexes.forEach(invalidRow => {
          const rowNumber = invalidRow.rowIndex + 1;
          const error = `Row ${rowNumber} contains ${invalidRow.rowLength} cells when there should be 9`;
          reportItem.errorMessages.push(error);
        });
        return reportItem;
      },
      characterValidation: errorData => {
        const reportItem = {
          errorName: "Invalid Characters",
          errorMessages: []
        };

        Object.keys(errorData.characterValidation.invalidCharacters).forEach(
          errorDataItem => {
            const invalidCharacters = errorData.characterValidation.invalidCharacters[
              errorDataItem
            ].map(item => item.character);
            const error = `Invalid characters at row ${parseInt(
              errorDataItem.split("_")[1],
              10
            ) + 1}: ${invalidCharacters.join(", ")}`;
            reportItem.errorMessages.push(error);
          }
        );
        return reportItem;
      }
    };

    return errorContentByValidationType[Object.keys(errorReport)[0]](
      errorReport
    );
  }
}

const errorHandling = new ErrorHandling();
