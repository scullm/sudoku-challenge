class DOMElements {
  addRow(tableIndex) {
    const resultsRow = document.createElement("div");
    resultsRow.className = "row";
    const resultsRowId = `results-row-for-table-${tableIndex}`;
    resultsRow.id = resultsRowId;
    document.getElementById("sudoku-results").appendChild(resultsRow);
    const hr = document.createElement("hr");
    document.getElementById("sudoku-results").appendChild(hr);
  }

  addTable(tableIndex, tableRows, side, status, headingText) {
    const resultsRowId = `results-row-for-table-${tableIndex}`;
    const contentBlock = document.createElement("div");
    contentBlock.className = "col-md-6";
    const contentBlockId = `table-${tableIndex}-row-${side}-side`;
    contentBlock.id = contentBlockId;
    document.getElementById(resultsRowId).appendChild(contentBlock);

    const contentBlockHeading = document.createElement("h2");
    contentBlockHeading.className = "text-" + status;
    contentBlockHeading.innerHTML = headingText;
    document.getElementById(contentBlockId).appendChild(contentBlockHeading);

    const sudokuTable = document.createElement("table");
    sudokuTable.className = "table table-bordered";
    tableRows.forEach((rowCells, rowIndex) => {
      const row = sudokuTable.insertRow(rowIndex);
      rowCells.forEach((cellCharacter, cellIndex) => {
        const cell = row.insertCell(cellIndex);
        cell.innerHTML = cellCharacter;
      });
    });

    document.getElementById(contentBlockId).appendChild(sudokuTable);
  }

  addErrors(formattedErrorReport, tableIndex) {
    const resultsRowId = `results-row-for-table-${tableIndex}`;

    const contentBlock = document.createElement("div");
    contentBlock.className = "col-md-6";
    const contentBlockId = `table-${tableIndex}-row-right-side`;
    contentBlock.id = contentBlockId;
    document.getElementById(resultsRowId).appendChild(contentBlock);

    const errorHeading = document.createElement("h2");
    errorHeading.className = "text-danger";
    errorHeading.innerHTML = `Error: ${formattedErrorReport.errorName}`;
    document.getElementById(contentBlockId).appendChild(errorHeading);

    formattedErrorReport.errorMessages.forEach(errorMessage => {
      const errorAlert = document.createElement("div");
      errorAlert.className = "alert alert-danger";
      errorAlert.innerHTML = errorMessage;
      document.getElementById(contentBlockId).appendChild(errorAlert);
    });
  }
}

const domElements = new DOMElements();
