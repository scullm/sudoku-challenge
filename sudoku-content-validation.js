class ContentValidation {
  constructor() {
    this.postValidationContent = [];
  }

  resolveValidation(sudokuTableContent) {
    this.initPostValidationContent(sudokuTableContent);

    const chunkedTableContent = sudokuTableContent.map(contentItem => {
      return this.resolveRowsAndColumnsForValidation(contentItem);
    });

    chunkedTableContent.forEach((chunkedTable, tableIndex) => {
      this.validateUniqueDigits(chunkedTable, tableIndex);
      this.validateRowLength(chunkedTable.rows, tableIndex);
      this.validateCharacters(chunkedTable.rows, tableIndex);
    });
    this.updateValidityResult();
  }

  updateValidityResult() {
    this.postValidationContent.forEach((table, index) => {
      const errorTypes = Object.keys(table.errorReport);
      if (errorTypes.length === 0) {
        this.postValidationContent[index].isValid = true;
      }
    });
  }

  validateUniqueDigits(chunked, tableIndex) {
    const validationName = "uniqueDigitsValidation";
    this.postValidationContent[tableIndex]["errorReport"][validationName] = {};
    chunked.rows.forEach((row, rowIndex) => {
      this.validateUniqueDigitsBySectionType(
        "row",
        rowIndex,
        row,
        tableIndex,
        validationName
      );
    });

    chunked.columns.forEach((column, columnIndex) => {
      this.validateUniqueDigitsBySectionType(
        "column",
        columnIndex,
        column,
        tableIndex,
        validationName
      );
    });

    const currentValidation = this.postValidationContent[tableIndex][
      "errorReport"
    ][validationName];
    const keysAtCurrentValidation = Object.keys(currentValidation);

    if (keysAtCurrentValidation.length === 0) {
      delete this.postValidationContent[tableIndex]["errorReport"][
        validationName
      ];
    }
  }

  validateUniqueDigitsBySectionType(
    sectionType,
    sectionIndex,
    section,
    tableIndex,
    validationName
  ) {
    const tI = tableIndex;
    const eR = "errorReport";
    const vN = validationName;

    const numericOnlySectionValues = section.filter(item => !isNaN(item));

    const sectionIndexInt = parseInt(sectionIndex, 10);
    const sectionNumber = sectionIndexInt + 1;
    const sN = sectionType + "_" + sectionNumber; // sectionName

    this.postValidationContent[tI][eR][vN][sN] = {};

    numericOnlySectionValues.forEach((itemValue, itemIndex) => {
      const dRN = "cellsWithValue_" + itemValue; // dataReportName

      if (
        typeof this.postValidationContent[tI][eR][vN][sN][dRN] === "undefined"
      ) {
        this.postValidationContent[tI][eR][vN][sN][dRN] = [];
      }
      const reportItem = { itemValue, itemIndex, sectionIndex, sectionType };
      this.postValidationContent[tI][eR][vN][sN][dRN].push(reportItem);
    });

    let sectionReportKeys = Object.keys(
      this.postValidationContent[tI][eR][vN][sN]
    );

    sectionReportKeys.forEach(k => {
      const isValidLength =
        this.postValidationContent[tI][eR][vN][sN][k].length > 1;
      if (!isValidLength) {
        delete this.postValidationContent[tI][eR][vN][sN][k];
      }
    });

    sectionReportKeys = Object.keys(this.postValidationContent[tI][eR][vN][sN]);

    if (sectionReportKeys.length === 0) {
      delete this.postValidationContent[tI][eR][vN][sN];
    }
  }

  validateRowLength(tableRows, tableIndex) {
    const tI = tableIndex;
    const eR = "errorReport";
    const vN = "rowLengthValidation"; // validationName
    this.postValidationContent[tI][eR][vN] = {};
    this.postValidationContent[tI][eR][vN].invalidRowIndexes = [];
    tableRows.forEach((row, rowIndex) => {
      const isValidRowLength = row.length === 9;
      if (!isValidRowLength) {
        const invalidRowLengthReport = {
          rowLength: row.length,
          rowIndex
        };
        this.postValidationContent[tI][eR][vN].invalidRowIndexes.push(
          invalidRowLengthReport
        );
      }
    });

    const sectionReportKeys = Object.keys(
      this.postValidationContent[tI][eR][vN].invalidRowIndexes
    );

    if (sectionReportKeys.length === 0) {
      delete this.postValidationContent[tI][eR][vN];
    }
  }

  validateCharacters(tableRows, tableIndex) {
    const tI = tableIndex;
    const eR = "errorReport";
    const vN = "characterValidation"; // validationName
    const rSN = "invalidCharacters"; // reportSectionName
    this.postValidationContent[tI][eR][vN] = {};
    this.postValidationContent[tI][eR][vN][rSN] = {};
    tableRows.forEach((row, rowIndex) => {
      const rId = `atRowIndex_${rowIndex}`;
      this.postValidationContent[tI][eR][vN][rSN][rId] = [];
      row.forEach(character => {
        var reg = /[^(1|2|3|4|5|6|7|8|9|.)]+/;
        if (reg.test(character)) {
          const invalidCharacterReport = {
            character,
            rowIndex
          };
          this.postValidationContent[tI][eR][vN][rSN][rId].push(
            invalidCharacterReport
          );
        }
      });
      if (this.postValidationContent[tI][eR][vN][rSN][rId].length === 0) {
        delete this.postValidationContent[tI][eR][vN][rSN][rId];
      }
    });

    const sectionReportKeys = Object.keys(
      this.postValidationContent[tI][eR][vN][rSN]
    );

    if (sectionReportKeys.length === 0) {
      delete this.postValidationContent[tI][eR][vN];
    }
  }

  resolveRowsAndColumnsForValidation(boardRows) {
    const data = {
      rows: boardRows,
      columns: []
    };

    boardRows.forEach(row => {
      row.forEach((rowItem, index) => {
        if (typeof data.columns[index] === "undefined") {
          data.columns[index] = [];
        }

        data.columns[index].push(rowItem);
      });
    });

    return data;
  }

  initPostValidationContent(sudokuTableContent) {
    sudokuTableContent.forEach(contentItem => {
      const initialContentItem = {
        rows: {
          solved: [],
          unsolved: contentItem
        },
        isValid: false,
        errorReport: {}
      };
      this.postValidationContent.push(initialContentItem);
    });
  }
}

const contentValidation = new ContentValidation();
